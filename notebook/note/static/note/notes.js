Ext.onReady(function() { // TODO: вынести json на onReady
    // Store
    var storeListNotes = new Ext.data.JsonStore({ //TODO: переделать на отдельную хранилку контента
        url: 'list',
        root: 'notes',
        totalProperty: 'count',
        fields: ['uuid', 'title', 'content', 'pub_date', 'favorite', 'public', 'category']
    });
    var storeCategory = new Ext.data.JsonStore({
        url: 'category',
        root: 'category',
        totalProperty: 'count',
        fields: ['id', 'name']
    });

    // Create Note
    var createForm = {
        xtype: 'form',
        id: 'create-form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        url:'create',
        items: [
        {
            xtype: 'textfield',
            id: 'create-title',
            fieldLabel: 'Title',
            allowBlank: false,
            anchor: '95%',
            blankText:'',
            minLength: 3,
            maxLength: 32,
            msgTarget:'side'
        },
        {
            xtype: 'textarea',
            fieldLabel: 'Content',
            id: 'create-content',
            anchor: '95%',
            height: 150,
            allowBlank: false
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Favorite',
            id: 'create-favorite',
            anchor: '40%'
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Public',
            id: 'create-public',
            anchor: '40%'
        },{
            xtype: 'combo',
            id: 'create-category',
            store: storeCategory,
            hiddenName: 'create-category-hidden',
            displayField: 'name',
            valueField: 'id',
            editable: false,
            mode: 'local',
            allowBlank: false,
            forceSelection: true,
            triggerAction: 'all',
            fieldLabel: 'Category',
            emptyText: 'Select a category...',
            selectOnFocus: true
        }],
        buttons: [
        {
            text: 'Cancel',
            handler: function() {
                createWindow.hide();
            }
        },{
            text: 'Create',
            handler: function() {
                Ext.getCmp('create-form').getForm().submit();
                storeListNotes.load(); //TODO: что-то тут не так
                gridNotes.getView().refresh();
                createWindow.hide();
            }
        }]
    };
    createWindow = new Ext.Window({
        layout: 'form',
        title: 'Create Note',
        width: 550,
        autoHeight: true,
        closeAction: 'hide',
        items: [createForm]
    });

    // Create Category
    var categoryForm = {
        xtype: 'form',
        id: 'category-form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        url:'createCategory',
        items: [{
            xtype: 'textfield',
            id: 'category',
            fieldLabel: 'Category',
            allowBlank: false,
            blankText:'Enter your category',
            minLength: 3,
            maxLength: 32,
            msgTarget:'side'
        }],
        buttons: [{
            text: 'Create',
            handler: function() {
                Ext.getCmp('category-form').getForm().submit();
                storeCategory.load();
                categoryWindow.hide();
            }
        },
        {
            text: 'Cancel',
            handler: function() {
                categoryWindow.hide();
            }
       }]
    };
    categoryWindow = new Ext.Window({
        layout: 'form',
        title: 'New Category',
        width: 330,
        autoHeight: true,
        closeAction: 'hide',
        items: [categoryForm]
    });

    // Edit note
    var editForm =  new Ext.FormPanel({
        xtype: 'form',
        id: 'edit-form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        url:'edit',
        items: [{
            xtype: 'hidden',
            id: 'edit-uuid',
            name: 'uuid'
        },
        {
            xtype: 'textfield',
            id: 'edit-title',
            name: 'title',
            fieldLabel: 'Title',
            allowBlank: false,
            anchor: '95%',
            blankText:'',
            minLength: 3,
            maxLength: 32,
            msgTarget:'side'
        },
        {
            xtype: 'textarea',
            fieldLabel: 'Content',
            name: 'content',
            id: 'edit-content',
            anchor: '95%',
            height: 150,
            allowBlank: false
        },
        {
            xtype: 'checkbox',
            name: 'favorite',
            fieldLabel: 'Favorite',
            id: 'edit-favorite',
            anchor: '40%'
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Public',
            name: 'public',
            id: 'edit-public',
            anchor: '40%'
        },{
            xtype: 'combo', // TODO: шляпа с подгрузкой категории
            id: 'edit-category',
            name: 'category',
            store: storeCategory,
            hiddenName: 'edit-category-hidden',
            displayField: 'name',
            valueField: 'id',
            editable: false,
            mode: 'local',
            allowBlank: false,
            forceSelection: true,
            triggerAction: 'all',
            fieldLabel: 'Category',
            emptyText: 'Select a category...',
            selectOnFocus: true
        }],
        buttons: [
        {
            text: 'Cancel',
            handler: function() {
                editWindow.hide();
            }
        },{
            text: 'edit',
            handler: function() {
                Ext.getCmp('edit-form').getForm().submit();
                storeListNotes.load(); //TODO: что-то тут не так
                gridNotes.getView().refresh();
                editWindow.hide();
            }
        }]
    });
    editWindow = new Ext.Window({
        layout: 'form',
        title: 'Edit Note',
        width: 550,
        autoHeight: true,
        closeAction: 'hide',
        items: [editForm]
    });

    // Tab panel. Content
    var content=new Ext.TabPanel({
        resizeTabs: true,
        region: 'center',
        minTabWidth: 115,
        tabWidth: 135,
        enableTabScroll: true,
        defaults: { autoScroll: true },
        activeTab: 0,
        items: [{
            title: 'Hello',
            html: 'double-click to open a note '
        }]
    });
    function addTab(record) {
        var tab=content.add({
            title: record.data.title,
            iconCls: 'icon-tab',
            bodyStyle:'padding: 5px',
            html: record.data.content + '<br/><br/>',
            closable: true
        });
        tab.show();
    };

    // Grid. Notes

    var gridNotes = new Ext.grid.GridPanel({
        region: 'south',
        title: 'Notes',
        collapsible: true,
        store: storeListNotes,
        columns: [{
            id: 'title-col',
            header: "Title",
            width: 500,
            autoSizeColumn: true,
            dataIndex: 'title',
            sortable: true,
        },{
            header: "Pub date",
            dataIndex: 'pub_date',
            width: 200,
            autoSizeColumn: true,
            sortable: true,
        },{
            header: "favorite",
            dataIndex: 'favorite',
            autoSizeColumn: true,
            sortable: true,
        },{
            header: "public",
            dataIndex: 'public',
            autoSizeColumn: true,
            sortable: true,
        },{
            header: "category",
            dataIndex: 'category',
            width: 200,
            autoSizeColumn: true,
            sortable: true,
            align: 'center'
        }],
        autoExpandColumn: 'title-col',
        loadMask: true,
        height: 300,
        minHeight: 100,
        bbar: new Ext.PagingToolbar({
            pageSize: 10,
            store: storeListNotes,
            displayInfo: true,
            displayMsg: 'Displaying movies {0} - {1} of {2}',
            emptyMsg: "No movies found"
        }),
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        listeners: {
            rowdblclick: function(searchgrid, rowIndex, e) {
                var record = storeListNotes.getAt(rowIndex);
                addTab(record);
            }
        },
        tbar: new Ext.Toolbar({
            items: [{
                text: 'Create Note',
                handler: function() {
                    createWindow.show();
                }
            },{
                text: 'Create category',
                handler: function() {
                    categoryWindow.show();
                }
            },{
                text: 'Edit',
                handler: function() {
                    var record = gridNotes.getSelectionModel().getSelected();
                    if(record) {
                        editForm.getForm().load({
                            url: 'getNote',
                            method: 'get',
                            params: {uuid: record.data.uuid},
                            waitMsg: 'Loading'
                        });
                        editWindow.show();
                    }
                    else
                        Ext.Msg.alert('Warning', 'row not selected');
                }
            },{
                text: 'View',
                handler: function() {
                    var record = gridNotes.getSelectionModel().getSelected();
                    if(record) {
                        var view_url = 'view?uuid=' + record.data.uuid;
                        window.open(view_url);
                    }
                    else
                        Ext.Msg.alert('Warning', 'row not selected');
                }
            },{
                text: 'Link',
                handler: function() {
                    var record = gridNotes.getSelectionModel().getSelected();
                    if(record) {
                        if (record.data.public)
                            var message = document.URL + 'view?uuid=' + record.data.uuid;
                        else
                            var message = 'Note has not been published';
                        Ext.Msg.alert('Link', message);
                    }
                    else
                        Ext.Msg.alert('Warning', 'row not selected');
                }
            },{
                text: 'Delete',
                handler: function() {
                    var record = gridNotes.getSelectionModel().getSelected();
                    if(record) {
                        Ext.Ajax.request({
                            url: 'delete',
                            method: 'post',
                            params: {uuid: record.data.uuid},
                        });
                        storeListNotes.load(); //TODO: что-то тут не так
                        gridNotes.getView().refresh();
                    }
                    else
                        Ext.Msg.alert('Warning', 'row not selected');
                }
            }]
        })
    });
    storeCategory.load();
    storeListNotes.load();

    // Title
    var title = {
        region: 'north',
        html: '<h1 class="x-panel-header">Welcome!</h1>',
        autoHeight: true,
        border: false,
        margins: '0 0 5 0'
    };

    // Viewport
    var layout = new Ext.Viewport({
        layout: 'border',
        items: [
            title,
            content,
            gridNotes
        ]
    });
});