from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('notebook.note.views',
    url(r'^$', 'index'),
    url(r'^list', 'list_notes'),
    url(r'^getNote', 'get_note'),
    url(r'^category', 'list_category'),
    url(r'^createCategory', 'create_category'),
    url(r'^create', 'create_note'),
    url(r'^edit', 'edit_note'),
    url(r'^delete', 'delete_note'),
    url(r'^view', 'view_note'),
)
