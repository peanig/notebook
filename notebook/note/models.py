from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    def as_json(self):
        return dict(id=self.id, name=self.name)


class Note(models.Model):
    uuid = models.CharField(max_length=36, primary_key=True)
    title = models.CharField(max_length=200)
    user = models.ForeignKey(User)
    content = models.TextField(max_length=200)
    pub_date = models.DateTimeField(auto_now_add=True)
    favorite = models.BooleanField()
    public = models.BooleanField()
    category = models.ForeignKey(Category)

    def as_json_info(self):
        return dict(
            uuid=self.uuid,
            title=self.title,
            pub_date=self.pub_date,
            favorite=self.favorite,
            category=self.category.name)

    def as_json_full(self):
        return dict(
            uuid=self.uuid,
            title=self.title,
            content=self.content,
            pub_date=self.pub_date,
            favorite=self.favorite,
            public=self.public,
            category=self.category.name)

    def __unicode__(self):
        return self.title
