from notebook.note.models import Note, Category
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json
import uuid


def index(request):
    return render_to_response('note/index.html', {'user': request.user})


def view_note(request):
    uuid = request.GET['uuid']
    note = Note.objects.get(uuid=uuid)
    if note.user == request.user or note.public:
        return render_to_response('note/view.html', {'title': note.title, 'content': note.content})
    else:
        return render_to_response('note/view.html', {'title': 'Sorry', 'content': 'Note has not been published'})


def list_notes(request):
    resultset = Note.objects.filter(user=request.user)  # TODO: change for current user
    notes = [ob.as_json_full() for ob in resultset]
    response_json = dict(count=resultset.__len__(),
                         notes=notes)
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")


def get_note(request):
    note = Note.objects.get(uuid=request.GET['uuid']).as_json_full();
    response_json = dict(success="true",
                         data=note)
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")


def list_category(request):
    resultset = Category.objects.all()
    category = [ob.as_json() for ob in resultset]
    response_json = dict(count=resultset.__len__(),
                         category=category)
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")


def edit_note(request):
    if request.method == 'GET':
        return render_to_response('note/index.html', {'user': request.user})
    if request.method == 'POST':
        uuid = request.POST['uuid']
        note = Note.objects.get(uuid=uuid)
        note.title = request.POST['title']
        note.content = request.POST['content']
        category_id = request.POST['edit-category-hidden']
        note.category = Category.objects.get(id=category_id)
        note.public = False if 'public' not in request.POST else True
        note.favorite = False if 'favorite' not in request.POST else True
        note.save()
        response_json = dict(success='True')
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")


def create_category(request):
    if request.method == 'GET':
        return render_to_response('note/index.html', {'user': request.user})
    if request.method == 'POST':
        name = request.POST['category']
        category = Category(name=name)
        category.save()
        response_json = dict(success='True')
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")

def delete_note(request):
    if request.method == 'GET':
        return render_to_response('note/index.html', {'user': request.user})
    if request.method == 'POST':
        uuid = request.POST['uuid']
        note = Note.objects.get(uuid=uuid)
        note.delete()
        response_json = dict(success='True')
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")

def create_note(request):
    if request.method == 'GET':
        return render_to_response('note/index.html', {'user': request.user})
    if request.method == 'POST':
        title = request.POST['create-title']
        content = request.POST['create-content']
        category_id = request.POST['create-category-hidden']
        public = False if 'create-public' not in request.POST else True
        favorite = False if 'create-favorite' not in request.POST else True
        category = Category.objects.get(id=category_id)
        note = Note(uuid=str(uuid.uuid1()), title=title, content=content, category=category,
                    user=request.user, public=public, favorite=favorite)
        note.save()
        response_json = dict(success='True')
    return HttpResponse(json.dumps(response_json, cls=DjangoJSONEncoder), content_type="application/json")