from notebook.note.models import Note,Category
from django.contrib import admin

admin.site.register(Note)
admin.site.register(Category)