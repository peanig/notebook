Ext.QuickTips.init();

Ext.apply(Ext.form.VTypes, {
    password: function(val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },
    passwordText: 'What are you doing?<br/>The passwords entered do not match!'
});

var signupForm = { xtype: 'form',
    id: 'register-form',
    labelWidth: 125,
    standardSubmit:true,
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    url: 'signup',
    items: [
        {
            xtype: 'textfield',
            id: 'signup-user',
            fieldLabel: 'Username',
            allowBlank: false,
            blankText:'Enter username',
            minLength: 3,
            maxLength: 32,
            anchor:'90%',
            msgTarget:'side'
        },
        {
            xtype: 'textfield',
            id: 'signup-email',
            fieldLabel: 'Email',
            allowBlank: false,
            minLength: 3,
            maxLength: 64,
            anchor:'90%',
            vtype:'email',
            msgTarget:'side',
        },
        {
            xtype: 'textfield',
            id: 'signup-pwd',
            fieldLabel: 'Password',
            inputType: 'password',
            allowBlank: false,
            minLength: 5,
            maxLength: 32,
            anchor:'90%',
            minLengthText: 'Password must be at least 5 characters long.',
            msgTarget:'side',
        },
        {
            xtype: 'textfield',
            id: 'signup-pwd-confirm',
            fieldLabel: 'Confirm Password',
            inputType: 'password',
            allowBlank: false,
            minLength: 5,
            maxLength: 32,
            anchor:'90%',
            minLengthText: 'Password must be at least 5 characters long.',
            msgTarget:'side',
            vtype: 'password',
            initialPassField: 'signup-pwd'
        }
    ],
    buttons: [{
        text: 'Register',
        handler: function() {
            Ext.getCmp('register-form').getForm().submit();
        }
    },
    {
        text: 'Login',
        handler: function() {
            signupWindow.hide();
            loginWindow.show();
        }
   }]
};
var loginForm = {
    xtype: 'form',
    id: 'login-form',
    bodyStyle: 'padding:15px;background:transparent',
    standardSubmit:true,
    border: false,
    url:'login',
    items: [
    {
        xtype: 'textfield',
        id: 'login-user',
        fieldLabel: 'Username',
        allowBlank: false,
        blankText:'Enter your username',
        minLength: 3,
        maxLength: 32,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'login-pwd',
        fieldLabel: 'Password',
        inputType: 'password',
        allowBlank: false,
        minLength: 5,
        maxLength: 32,
        msgTarget:'side',
        minLengthText: 'Password must be at least 5 characters long.'
    }],
    buttons: [{
        text: 'Enter',
        handler: function() {
            Ext.getCmp('login-form').getForm().submit();
        }
    },
    {
        text: 'Signup',
        handler: function() {
            loginWindow.hide();
            signupWindow.show();
        }
   }]
};

Ext.onReady(function () {
    signupWindow = new Ext.Window({
        layout: 'form',
        title: 'Sign up',
        width: 340,
        autoHeight: true,
        closeAction: 'hide',
        items: [signupForm]
    });
    loginWindow = new Ext.Window({
        layout: 'form',
        title: 'Log in',
        width: 340,
        autoHeight: true,
        closeAction: 'hide',
        items: [loginForm]
    });

    loginWindow.show();
    signupWindow.hide();
});

