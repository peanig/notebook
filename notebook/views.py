from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

def index(request):
    if request.method == 'GET':
        return render_to_response('main/login.html')
    if request.method == 'POST':
        username = request.POST['login-user']
        password = request.POST['login-pwd']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('/note/')
    return render_to_response('main/login.html')

def signup(request):
    if request.method == 'GET':
        return render_to_response('main/login.html')
    if request.method == 'POST':
        username = request.POST['signup-user']
        password = request.POST['signup-pwd']
        email = request.POST['signup-email']
        user = User.objects.create_user(username, email, password)
        user.save()
        return redirect('/note/')
    return render_to_response('main/login.html')