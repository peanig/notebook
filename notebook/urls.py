from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'notebook.views.index', name='index'),
    url(r'^login', 'notebook.views.index', name='index'),
    url(r'^signup', 'notebook.views.signup', name='signup'),
    url(r'^note/', include('notebook.note.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
